# README #

# package and execute #


mvn clean && mvn compile && mvn package && \
spark-submit \
	--class spark.test.WordCount \
	/home/lorenzo/workspace/TestSpark/target/test-0.0.1-SNAPSHOT.jar \
	/home/lorenzo/Documents/sparkTest/appunti.txt /home/lorenzo/Documents/sparkTest/wordcounts
	
	
# Jar without dependencies 
mvn clean && mvn compile && mvn package && \
spark-submit --packages com.databricks:spark-csv_2.10:1.4.0 \
	--class spark.test.App \
	/home/lorenzo/workspace/TestSpark/target/test-0.0.1-SNAPSHOT.jar


# Jar with external libraries included
mvn clean && mvn compile && mvn package && \
spark-submit \
	--class spark.test.App \
	/home/lorenzo/workspace/TestSpark/target/test-0.0.1-SNAPSHOT.jar