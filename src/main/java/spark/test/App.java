package spark.test;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.rdd.JdbcRDD;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        
        // Create a Java Spark Context.
        SparkConf conf = new SparkConf().setAppName("wordCount");
        JavaSparkContext sc = new JavaSparkContext(conf);

        SparkSession spark = SparkSession
        		.builder()
        		.appName("Java Spark SQL Example")
        		.getOrCreate();

        
        String fileName = "/home/lorenzo/Documents/sparkTest/test.csv";
        String tableName = "testTab"; 
        
		loadFileAndCreateView(spark, fileName, tableName);
        
//		String sql = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY codicecliente) as row FROM STATISTICHE_PFP ) a WHERE row > ? and row <= ?";
//		JavaRDD<Object[]> testJdbc = JdbcRDD.<Object[]>create(sc, 
//				new MyConnectionFactory(), sql, 0l, 2000000, 10,
//					new Function<java.sql.ResultSet, Object[]>() {
//						@Override
//						public Object[] call(ResultSet row) throws Exception {
//							return JdbcRDD.resultSetToObjectArray(row);
//						}
//					}
//				);
// 		System.out.println("MEGA COUNT: " + testJdbc.count());
        
 		List<String> list = Arrays.asList("ciao", "pippo");
 		JavaRDD<String> listRDD = sc.parallelize(list);
 		
		
		long currentTimeMillis = System.currentTimeMillis();
// 		Dataset<Row> queryRes = spark.sql("SELECT FORMATECNICA, count(*) as tot FROM testTab group by FORMATECNICA order by tot desc");
		Dataset<Row> queryRes = spark.sql("SELECT * FROM testTab where codicecliente = 1178161");
 		queryRes.rdd().toJavaRDD().collect().forEach(System.out::println);
 		System.out.println(System.currentTimeMillis() - currentTimeMillis);
    }

    private static class MyConnectionFactory implements JdbcRDD.ConnectionFactory {
		public Connection getConnection() {
	        try {
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
				return DriverManager.getConnection("jdbc:sqlserver://wmpf-ampro64.prometeia:1433;databaseName=BUD-BPER-SVILUPPO;username=ampro;password=ampro");
			} catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException e) {
				e.printStackTrace();
			}
	        throw new IllegalStateException();
		}
    }

	private static void loadFileAndCreateView(SparkSession spark, String fileName, String tableName) {
		
		String parquetFilePath = "/tmp/spark/parquet/";
		
		Dataset<Row> data = spark.read()
        		.format("com.databricks.spark.csv")
        		.option("header", "true")
        		.option("inferSchema", "true")
//        		.option("treatEmptyValuesAsNulls","true")
        		.option("delimiter", ";")
        		.load(fileName);
		
		String parquetFile = parquetFilePath + tableName;
		
		File f = new File(parquetFile);
        try {
        	if (f.exists())
        		FileUtils.forceDelete(f);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		data.write().parquet(parquetFile);
		data = spark.read().parquet(parquetFile);
        
 		data.createOrReplaceTempView(tableName);
	}
}
