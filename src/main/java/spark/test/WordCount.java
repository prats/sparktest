package spark.test;

import java.io.File;
import java.util.Arrays;

import org.apache.commons.io.FileUtils;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import scala.Tuple2;

public class WordCount {
	public static void main(String[] args) throws Exception {
		String inputFile = args[0];
		String outputFile = args[1];
		
		File f = new File(outputFile);
        FileUtils.cleanDirectory(f); //clean out directory (this is optional -- but good know)
        FileUtils.forceDelete(f); //delete directory
		
		// Create a Java Spark Context.
		SparkConf conf = new SparkConf().setAppName("wordCount");
		JavaSparkContext sc = new JavaSparkContext(conf);
		
		// Load our input data
		JavaRDD<String> input = sc.textFile(inputFile);
		// Split up into words.
		JavaRDD<String> words = input.flatMap(x -> Arrays.asList(x.split(" ")).iterator());
		// Transform into word and count.
		JavaPairRDD<Integer, String> counts = 
				words.mapToPair(x -> new Tuple2<String, Integer>(x, 1))
					.reduceByKey((x,y) -> x + y )
					.mapToPair(x -> x.swap()) // swap key with value for sorting
					.sortByKey(false);
		// Save the word count back out to a text file, causing evaluation.
		counts.coalesce(1, true).saveAsTextFile(outputFile);
		
		sc.close();
	}
}